import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int i = 1;
        int answer1 = 1;
        int answer2 = 1;

        System.out.println("Enter a number: ");
        int num = in.nextInt();

        if (num < 0) {
            System.out.println("Please enter a positive integer: ");
        } else {

            try {
                while (i <= num) {
                    answer1 *= i;
                    i++;
                }
//                System.out.println(answer1);

                for (int j = 1; j <= num; ++j) {
                    answer2 *= j;
                }
//                System.out.println(answer2);

            } catch (Exception e) {
                System.out.println("Invalid input");
                e.printStackTrace();
            } finally {
                if (answer1 == answer2) {
                    System.out.println("The factorial of " + num + " is: " + answer1);
                } else {
                    System.out.println("Error");
                }
            }

        }
    }
}